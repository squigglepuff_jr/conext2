#ifndef VFS_H
#define VFS_H

#include "inode.h"
#include "superblock.h"

/*!
 * \brief OpenExt2Fs - Open EXT2 Filesystem
 *
 * This function will try to open a filesystem located at "apPath". This filesystem must be a valid Ext2/3/4 filesystem with a valid superblock and not errors.
 *
 * \param apPath String representing the filepath to the filesystem.
 *
 * \note Does not throw exceptions!
 * \return ext2_filsys object containing the open filesystem or nullptr if failed.
 */
ext2_filsys OpenExt2Fs(const char* apPath) noexcept;

/*!
 * \brief CloseExt2Fs - Closes an open VFS handle.
 * \param atFileSys Valid and open Ext2 FS handle to be closed.
 */
void CloseExt2Fs(ext2_filsys atFileSys);

/*!
 * \brief CreateDirectory - Create a new directory inode and it's parent reference.
 *
 * This function will create a new inode for the directory as needed and link it to it's own name along with it's parent. This ensures that "." and ".." function correctly.
 *
 * \note This function will overwrite an existing inode and intern, abandon any links/data therein!
 * \note apOutInode will be overidden! Don't pass anything you want to keep!
 *
 * \param asDirName String representing the directory name.
 * \param atFileSys Valid and open Ext2 FS handle.
 * \param aiParent Index of the parent inode.
 * \param aiIndex Index of this inode.
 * \param apOutInode A valid ext2_inode_large handle that'll be populated with the new inode (used for linking).
 *
 * \exception std::system_error C++11 system_error containing error code and representing string.
 * \return Integer reflective of the ext2fs library return codes.
 */
long CreateDirectory(std::string asDirName, ext2_filsys atFileSys, ext2_ino_t aiParent, ext2_ino_t aiIndex, ext2_inode_large* apOutInode);

/*!
 * \brief PopulatetVFS - Populate a newly created VFS.
 *
 * This function will take the currently open VFS handle and populate it with files in asListPath.
 *
 * \param asListPath String containing the path to the ordered file list to populate the VFS with.
 * \param atFileSys Valid and open Ext2 FS handle.
 * \param asRoot String containing the base "sysroot" path of the files specified in the file specified at asListPath.
 *
 * \exception std::system_error C++11 system_error containing error code and representing string.
 * \return Integer reflective of the ext2fs library return codes.
 */
long PopulatetVFS(std::string asListPath, ext2_filsys atFileSys, std::string asRoot="/");

/*!
 * \brief CreateNewVFS - Create a new Ext2 VFS on the open filesystem.
 *
 * This function will create a new VFS on the given ext2 filesystem handle.
 * \note This will discard and forcably override any existing superblock and table data!
 *
 * \param atFileSys  Valid and open Ext2 FS handle.
 *
 * \exception std::system_error C++11 system_error containing error code and representing string.
 * \return Integer reflective of the ext2fs library return codes.
 */
long CreateNewVFS(ext2_filsys atFileSys, std::string asFsName, const byte asUUID[16]);

/*!
 * \brief AllocateReservedInodes - Allocate new reserved inodes for a new VFS.
 *
 * This function will allocate a new set of reserved inodes. These inodes are as follows:
 * 1 - Bad Blocks inode
 * 2 - Root Inode
 * 3 - ACL Index inode (deprecated/unused)
 * 4 - ACL data inode (deprecated/unused)
 * 5 - Boot loader inode
 * 6 - Undeleted directory inode
 * 7 - Resize inode
 * 8 - Journal inode (unused)
 * 9 - Excluded blocks inode
 * 10 - Replicated blocks inode
 *
 * Inode 11 (EXT2_GOOD_OLD_FIRST_INO) is the first inode that can used by the regular filesystem.
 *
 * \param atFileSys  Valid and open Ext2 FS handle.
 *
 * \exception std::system_error C++11 system_error containing error code and representing string.
 * \return Integer reflective of the ext2fs library return codes.
 */
long AllocateReservedInodes(ext2_filsys atFileSys);

/*!
 * \brief CreateLostAndFound - Create "lost+found" for the filesystem.
 *
 * This function will create the lost+found directory for the filesystem and set it's permissions/ownership correctly.
 *
 * \param atFileSys  Valid and open Ext2 FS handle.
 *
 * \exception std::system_error C++11 system_error containing error code and representing string.
 * \return Integer reflective of the ext2fs library return codes.
 */
long CreateLostAndFound(ext2_filsys atFileSys);

/*!
 * \brief PurgeInodes - Remove ALL inodes from filesystem.
 *
 * This function will systematically go through all the inodes in the system and remove them. This is needed so the filesystem is clean.
 * \note This will REMOVE the root inode! Be sure to call "CreateRoot()" to create a new root inode!
 *
 * \param atFileSys Valid and open Ext2 FS handle.
 *
 * \exception std::system_error C++11 system_error containing error code and representing string.
 * \return Integer reflective of the ext2fs library return codes.
 */
long PurgeInodes(ext2_filsys atFileSys);

#endif // VFS_H
