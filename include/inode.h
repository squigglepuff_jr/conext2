#ifndef INODE_H
#define INODE_H

#include "error_map.h"

/*!
 * \brief CreateInode - Create a new inode in the filesystem.
 *
 * This function will make a new inode and place it in apOutInode. This doesn't fill in any data or set any fields for the inode itself. This function also won't link the inodes to each other or directory entries (dentry).
 *
 * \note This function will overwrite an existing inode and intern, abandon any links/data therein!
 * \note apOutInode will be overidden! Don't pass anything you want to keep!
 *
 * \param atFileSys Valid and open Ext2 FS handle.
 * \param aiParent Index of the parent inode.
 * \param aiIndex Index of this inode.
 * \param apOutInode A valid ext2_inode_large handle that'll be populated with the new inode (used for linking).
 *
 * \exception std::system_error C++11 system_error containing error code and representing string.
 * \return Integer reflective of the ext2fs library return codes.
 */
long CreateInode(ext2_filsys atFileSys, ext2_ino_t aiParent, ext2_ino_t *aiIndex, ext2_inode_large* apOutInode);

/*!
 * \brief WriteInode - Write an inode to the filesystem and flush the filesystem.
 *
 * This function will write the given inode to the filesystem object and then flush the FS to the disk.
 *
 * \param atFileSys Valid and open Ext2 FS handle.
 * \param atInode A valid ext2_inode_large handle.
 * \param atInodeIdx Index of the inode to write.
 * \param abSkipNew Should we skip writing a "new" inode?
 *
 * \exception std::system_error C++11 system_error containing error code and representing string.
 * \return Integer reflective of the ext2fs library return codes.
 */
long WriteInode(ext2_filsys atFileSys, ext2_inode_large atInode, ext2_ino_t atInodeIdx, bool abSkipNew = false);

/*!
 * \brief CreateRoot - Create a new root inode at EXT2_ROOT_INO (2).
 *
 * This function will create a new root inode at the EXT2_ROOT_INO index (2).
 *
 * \note This function will overwrite an existing inode and intern, abandon any links/data therein!
 * \note apOutInode will be overidden! Don't pass anything you want to keep!
 *
 * \param atFileSys Valid and open Ext2 FS handle.
 * \param apOutInode A valid ext2_inode_large handle that'll be populated with the new inode (used for linking).
 *
 * \exception std::system_error C++11 system_error containing error code and representing string.
 * \return Integer reflective of the ext2fs library return codes.
 */
long CreateRoot(ext2_filsys atFileSys, ext2_inode_large *apOutInode);

/*!
 * \brief CloneOwnerPerms - "Clone" an inode's ownership/permissions into a blank inode.
 *
 * This function will effectively "clone" an inode from a pre-existing file into a newly allocated inode. This is done by performing a stat() on the inode at asRoot + asFilePath, then manually
 * populating the fields of the newly allocated inode with the correct information.
 *
 * \note This will only clone ownership and filemode for now. We don't care about the other fields.
 *
 * \param atDestInode - Pointer referencing the destination inode to dump info into.
 * \param asFilePath - The file name to be stat'd
 * \param asRoot - The root to the "sysroot" of the file (must be a directory!)
 *
 * \exception std::system_error C++11 system_error containing error code and representing string.
 * \return Boolean, True means success. False means failure and errno is set appropriately.
 */
bool CloneOwnerPerms(ext2_inode_large* atDestInode, std::string asFilePath, std::string asRoot="/");

/*!
 * \brief ResetInodeTimestamps - Reset timestamps for the provided inode.
 *
 * This function will reset the timestamps for the provided inode to 1.
 * \note Resetting to 0 causes the underlying library to auto-insert the current timestamp.
 *
 * \param atFileSys Valid and open Ext2 FS handle.
 *
 * \exception std::system_error C++11 system_error containing error code and representing string.
 * \return Integer reflective of the ext2fs library return codes.
 */
long ResetInodeTimestamps(ext2_filsys atFileSys);

#endif // INODE_H
