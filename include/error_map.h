#ifndef ERROR_MAP_H
#define ERROR_MAP_H

#include "helpers.h"

/*!
 * \brief g_mErrMap Error string map for libext2 error codes.
 */
extern std::map<long, std::string> g_mErrMap;

#endif // ERROR_MAP_H
