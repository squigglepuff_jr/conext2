#ifndef SUPERBLOCK_H
#define SUPERBLOCK_H

#include "error_map.h"

/*!
 * \brief ResetSuperblockTimestamps - Reset timestamps for the provided inode.
 *
 * This function will reset the timestamps for the provided superblock to 1.
 * \note Resetting to 0 causes the underlying library to auto-insert the current timestamp.
 *
 * \param atFileSys Valid and open Ext2 FS handle.
 *
 * \exception std::system_error C++11 system_error containing error code and representing string.
 * \return Integer reflective of the ext2fs library return codes.
 */
long ResetSuperblockTimestamps(ext2_filsys atFileSys);

/*!
 * \brief SetHTreeSeed - Set the HTREE seed for directory hashing.
 *
 * This function will set the superblock's HTREE seed (16-bytes/128-bits) and flush the superblock data to the filesystem object.
 *
 * \param atFileSys Valid and open Ext2 FS handle.
 * \param alSeed Array of 4 unsigned integers to set the HTREE seed to.
 *
 * \exception std::system_error C++11 system_error containing error code and representing string.
 * \return Integer reflective of the ext2fs library return codes.
 */
long SetHTreeSeed(ext2_filsys atFileSys, unsigned int const alSeed[4]);

#endif // SUPERBLOCK_H
