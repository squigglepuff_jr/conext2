#ifndef GLOBAL_H
#define GLOBAL_H

#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <cassert>
#include <cerrno>
#include <fstream>
#include <exception>
#include <system_error>

// C++ STL Containers.
#include <vector>
#include <string>
#include <map>

// libe2fs
#include <ext2fs/ext2fs.h>

// Linux headers.
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <dirent.h>

// MACROS.
#if defined(__NO_CPP11_SUPPORT__)
#define nullptr (NULL)
#endif // #if defined(__NO_CPP11_SUPPORT__)

#if !defined(SZ)
#define SZ(x) (sizeof(x) / sizeof(*(x)))
#endif // #if !defined(SZ)

#if !defined(FORMAT_VER)
#define FORMAT_VER(maj, min, rev) ((maj<<24) | (min<<16) | (rev<<8) | 0x00)
#define MAJOR_VER(x) ((x & 0xff000000)>>24)
#define MINOR_VER(x) ((x & 0x00ff0000)>>16)
#define REV_VER(x) ((x & 0x0000ff00)>>8)
#endif

// Version information.
#define CONEXT2_VERSION FORMAT_VER(0, 1, 1)

// Custom types.
typedef unsigned char byte; //!< Exactly 8-bits.
typedef unsigned short word; //!< Exactly 16-bits.
typedef unsigned int dword; //!< Exactly 32-bits.
typedef unsigned long long qword; //!< Exactly 64-bits.

#endif // GLOBAL_H
