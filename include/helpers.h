#ifndef HELPERS_H
#define HELPERS_H

#include "global.h"

/*!
 * \brief ExtractFiles - Extract list from file.
 *
 * This function will attempt to open the file located at asListPath and read in the file list to be synchronized.
 * Each line represents a different entry to be synchronized.
 *
 * \param asListPath String holding the path to the file to be read.
 *
 * \exception std::system_error C++11 system_error containing error code and representing string.
 * \return std::vector<std::string> of entries to be synchronized.
 */
std::vector<std::string> ExtractFiles(std::string asListPath);

/*!
 * \brief DetermineFileType - Determine a synch entry's file type.
 *
 * This function will attempt to stat() the provided path located in the provided root and determine the filetype the new inode/dentry should be.
 *
 * \param asFilePath - The file name to be stat'd
 * \param asRoot - The root to the "sysroot" of the file (must be a directory!)
 *
 * \exception std::system_error C++11 system_error containing error code and representing string.
 * \return Integer code representing the underlying libext2fs type, or -1 if failure.
 */
int DetermineFileType(std::string asFilePath, std::string asRoot="/");

/*!
 * \brief ExtractLastEntry - Extracts the last element in a path.
 *
 * This function will extract the last element from a path by using the '/' character as a delimiter.
 *
 * \param asPath String of the path to parse.
 *
 * \exception std::system_error C++11 system_error containing error code and representing string.
 * \return std::string containing the last element or an empty string if failed.
 */
std::string ExtractLastEntry(std::string asPath) noexcept;

#endif // HELPERS_H
