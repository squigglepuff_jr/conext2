# ConExt2 Roadmap and TODO file.

## Summary
This file simply contains all the TODO's for the project. These are things that are planned but haven't been fully implemented and tested yet.

## TODOs
- [ ] Create initialize a VFS on a blank image
  - [ ] Initialize superblock.
  - [ ] Allocate tables/bitmaps.
  - [ ] Allocate reserved inodes (1-10).
  - [ ] Create "lost+found" directory.
- [ ] Allocate inodes, maintaining directory hierarchy outlined in passed in list.
- [ ] Parse command-line arguments similar to those for mke2fs.
- [ ] Allocate and copy file data from sysroot to new filesystem during inode allocation.

...

- [ ] Test Windows and MacOS environments and get them working (WAAAAY in the future if ever).
