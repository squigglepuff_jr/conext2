#include "inode.h"

/*
 * \brief CreateInode - Create a new inode in the filesystem.
 *
 * This function will make a new inode and place it in apOutInode. This doesn't fill in any data or set any fields for the inode itself. This function also won't link the inodes to each other or directory entries (dentry).
 *
 * \note This function will overwrite an existing inode and intern, abandon any links/data therein!
 * \note apOutInode will be overidden! Don't pass anything you want to keep!
 *
 * \param atFileSys Valid and open Ext2 FS handle.
 * \param aiParent Index of the parent inode.
 * \param aiIndex Index of this inode.
 * \param apOutInode A valid ext2_inode_large handle that'll be populated with the new inode (used for linking).
 *
 * \exception std::system_error C++11 system_error containing error code and representing string.
 * \return Integer reflective of the ext2fs library return codes.
 */
long CreateInode(ext2_filsys atFileSys, ext2_ino_t aiParent, ext2_ino_t *aiIndex, ext2_inode_large* apOutInode)
{
    assert(nullptr != atFileSys);
    assert(0 != aiParent);
    assert(nullptr != aiIndex);
    assert(nullptr != apOutInode);

    long iRtnCode = 1;

    // Allocate blocks for the new inode.
    blk_t iBlkGet = 0;
    iRtnCode = ext2fs_new_block(atFileSys, 1, atFileSys->block_map, &iBlkGet);
    if (0 != iRtnCode || 0 == iBlkGet)
    {
        std::error_code eCode{static_cast<int>(iRtnCode), std::generic_category()};
        throw std::system_error(eCode, g_mErrMap[iRtnCode]);
    }

    // Allocate a new inode.
    iRtnCode = ext2fs_new_inode(atFileSys, aiParent, 0, atFileSys->inode_map, aiIndex);
    if (0 != iRtnCode)
    {
        std::error_code eCode{static_cast<int>(iRtnCode), std::generic_category()};
        throw std::system_error(eCode, g_mErrMap[iRtnCode]);
    }

    // Retrieve the newly created inode.
    const unsigned int ciSize = sizeof(ext2_inode_large);
    iRtnCode = ext2fs_read_inode_full(atFileSys, *aiIndex, reinterpret_cast<ext2_inode*>(apOutInode), ciSize);
    if (0 != iRtnCode)
    {
        std::error_code eCode{static_cast<int>(iRtnCode), std::generic_category()};
        throw std::system_error(eCode, g_mErrMap[iRtnCode]);
    }

    // Add blocks to the inode.
    iRtnCode = ext2fs_iblk_add_blocks(atFileSys, reinterpret_cast<ext2_inode*>(&apOutInode), 1);
    if (0 != iRtnCode)
    {
        std::error_code eCode{static_cast<int>(iRtnCode), std::generic_category()};
        throw std::system_error(eCode, g_mErrMap[iRtnCode]);
    }

    // Set timestamps to a static value.
    (*apOutInode).i_atime = 1;
    (*apOutInode).i_mtime = 1;
    (*apOutInode).i_ctime = 1;
    (*apOutInode).i_crtime = 1;
    (*apOutInode).i_dtime = 0;
    (*apOutInode).i_atime_extra = 1;
    (*apOutInode).i_mtime_extra = 1;
    (*apOutInode).i_ctime_extra = 1;
    (*apOutInode).i_crtime_extra = 1;
    (*apOutInode).i_generation = 0x0;

    // Set 0:0 as owner (root:root).
    (*apOutInode).i_uid = 0x00;
    (*apOutInode).i_gid = 0x00;

    // Mark the inode bitmaps.
    iRtnCode = ext2fs_mark_inode_bitmap(atFileSys->inode_map, *aiIndex);
    if (0 != iRtnCode)
    {
        std::error_code eCode{static_cast<int>(iRtnCode), std::generic_category()};
        throw std::system_error(eCode, g_mErrMap[iRtnCode]);
    }

    return iRtnCode;
}

/*
 * \brief WriteInode - Write an inode to the filesystem and flush the filesystem.
 *
 * This function will write the given inode to the filesystem object and then flush the FS to the disk.
 *
 * \param atFileSys Valid and open Ext2 FS handle.
 * \param atInode A valid ext2_inode_large handle.
 * \param atInodeIdx Index of the inode to write.
 * \param abSkipNew Should we skip writing a "new" inode?
 *
 * \exception std::system_error C++11 system_error containing error code and representing string.
 * \return Integer reflective of the ext2fs library return codes.
 */
long WriteInode(ext2_filsys atFileSys, ext2_inode_large atInode, ext2_ino_t atInodeIdx, bool abSkipNew)
{
    assert(nullptr != atFileSys);

    long iRtnCode = 1;
    const unsigned int iSize = sizeof(ext2_inode_large);

    if (!abSkipNew)
    {
        // Write the new inode to the table!
        iRtnCode = ext2fs_write_new_inode(atFileSys, atInodeIdx, reinterpret_cast<ext2_inode*>(&atInode));
        if (0 != iRtnCode)
        {
            std::error_code eCode{static_cast<int>(iRtnCode), std::generic_category()};
            throw std::system_error(eCode, g_mErrMap[iRtnCode]);
        }
    }

    // Write the FULL inode now!
    iRtnCode = ext2fs_write_inode_full(atFileSys, atInodeIdx, reinterpret_cast<ext2_inode*>(&atInode), sizeof(iSize));
    if (0 != iRtnCode)
    {
        std::error_code eCode{static_cast<int>(iRtnCode), std::generic_category()};
        throw std::system_error(eCode, g_mErrMap[iRtnCode]);
    }

    // Write the bitmap.
    iRtnCode = ext2fs_write_bitmaps(atFileSys);
    if (0 != iRtnCode)
    {
        std::error_code eCode{static_cast<int>(iRtnCode), std::generic_category()};
        throw std::system_error(eCode, g_mErrMap[iRtnCode]);
    }

    // Clear the inode cache just to be safe.
    iRtnCode = ext2fs_flush_icache(atFileSys);
    if (0 != iRtnCode)
    {
        std::error_code eCode{static_cast<int>(iRtnCode), std::generic_category()};
        throw std::system_error(eCode, g_mErrMap[iRtnCode]);
    }

    // Flush the filesystem.
    iRtnCode = ext2fs_flush(atFileSys);
    if (0 != iRtnCode)
    {
        std::error_code eCode{static_cast<int>(iRtnCode), std::generic_category()};
        throw std::system_error(eCode, g_mErrMap[iRtnCode]);
    }

    return iRtnCode;
}

/*
 * \brief CreateRoot - Create a new root inode at EXT2_ROOT_INO (2).
 *
 * This function will create a new root inode at the EXT2_ROOT_INO index (2).
 *
 * \note This function will overwrite an existing inode and intern, abandon any links/data therein!
 * \note apOutInode will be overidden! Don't pass anything you want to keep!
 *
 * \param atFileSys Valid and open Ext2 FS handle.
 * \param apOutInode A valid ext2_inode_large handle that'll be populated with the new inode (used for linking).
 *
 * \exception std::system_error C++11 system_error containing error code and representing string.
 * \return Integer reflective of the ext2fs library return codes.
 */
long CreateRoot(ext2_filsys atFileSys, ext2_inode_large *apOutInode)
{
    assert(nullptr != atFileSys);
    assert(nullptr != apOutInode);

    long iRtnCode =  1;
    ext2_ino_t iIndex = EXT2_ROOT_INO;
    iRtnCode = CreateInode(atFileSys, 1, &iIndex, apOutInode);
    if (EXT2_ROOT_INO != iIndex)
    {
        std::error_code eCode{static_cast<int>(iRtnCode), std::generic_category()};
        throw std::system_error(eCode, g_mErrMap[iRtnCode]);
    }

    // Create the links.
    iRtnCode = ext2fs_link(atFileSys, EXT2_ROOT_INO, ".", EXT2_ROOT_INO, EXT2_FT_DIR);
    if (0 != iRtnCode)
    {
        std::error_code eCode{static_cast<int>(iRtnCode), std::generic_category()};
        throw std::system_error(eCode, g_mErrMap[iRtnCode]);
    }

    iRtnCode = ext2fs_link(atFileSys, EXT2_ROOT_INO, "..", EXT2_ROOT_INO, EXT2_FT_DIR);
    if (0 != iRtnCode)
    {
        std::error_code eCode{static_cast<int>(iRtnCode), std::generic_category()};
        throw std::system_error(eCode, g_mErrMap[iRtnCode]);
    }

    // Write the inode.
    iRtnCode = WriteInode(atFileSys, *apOutInode, EXT2_ROOT_INO);

    return iRtnCode;
}

/*
 * \brief CloneOwnerPerms - "Clone" an inode's ownership/permissions into a blank inode.
 *
 * This function will effectively "clone" an inode from a pre-existing file into a newly allocated inode. This is done by performing a stat() on the inode at asRoot + asFilePath, then manually
 * populating the fields of the newly allocated inode with the correct information.
 *
 * \note This will only clone ownership and filemode for now. We don't care about the other fields.
 *
 * \param atDestInode - Pointer referencing the destination inode to dump info into.
 * \param asFilePath - The file name to be stat'd
 * \param asRoot - The root to the "sysroot" of the file (must be a directory!)
 *
 * \exception std::system_error C++11 system_error containing error code and representing string.
 * \return Boolean, True means success. False means failure and errno is set appropriately.
 */
bool CloneOwnerPerms(ext2_inode_large* atDestInode, std::string asFilePath, std::string asRoot)
{
    assert(nullptr != atDestInode);
    assert(!asFilePath.empty());
    assert(!asRoot.empty());

    bool bSuccess = false;

    // Format the final path to stat().
    std::string sFinalPath = asRoot + asFilePath;

    // Stat the entry!
    struct stat tEntryStat;
    if (0 != stat(sFinalPath.data(), &tEntryStat))
    {
        std::error_code eCode{errno, std::generic_category()};
        throw std::system_error(eCode, strerror(errno));
    }

    // Begin populating the inode as needed.
    (*atDestInode).i_mode = static_cast<unsigned short>(tEntryStat.st_mode);
    (*atDestInode).i_uid = static_cast<unsigned short>(tEntryStat.st_uid);
    (*atDestInode).i_gid = static_cast<unsigned short>(tEntryStat.st_gid);
    (*atDestInode).i_atime = 1;
    (*atDestInode).i_mtime = 1;
    (*atDestInode).i_ctime = 1;
    (*atDestInode).i_crtime = 1;
    (*atDestInode).i_dtime = 0;
    (*atDestInode).i_atime_extra = 1;
    (*atDestInode).i_mtime_extra = 1;
    (*atDestInode).i_ctime_extra = 1;
    (*atDestInode).i_crtime_extra = 1;
    (*atDestInode).i_generation = 0x0;
    ++(*atDestInode).i_links_count;
    (*atDestInode).i_flags = EXT2_UNRM_FL | EXT2_SYNC_FL | EXT2_NOATIME_FL;// | EXT4_EXTENTS_FL;

    bSuccess = true;

    return bSuccess;
}

/*
 * \brief ResetInodeTimestamps - Reset timestamps for the provided inode.
 *
 * This function will reset the timestamps for the provided inode to 1.
 * \note Resetting to 0 causes the underlying library to auto-insert the current timestamp.
 *
 * \param atFileSys Valid and open Ext2 FS handle.
 *
 * \exception std::system_error C++11 system_error containing error code and representing string.
 * \return Integer reflective of the ext2fs library return codes.
 */
long ResetInodeTimestamps(ext2_filsys atFileSys)
{
    assert(nullptr != atFileSys);

    long iRtnCode = 1;
    const unsigned int iSize = sizeof(ext2_inode_large);
    ext2_inode_scan tInodeScan = nullptr;

    // Begin iterating over the list and creating ordered inodes!
    iRtnCode = ext2fs_open_inode_scan(atFileSys, 0, &tInodeScan);
    if (0 != iRtnCode)
    {
        std::error_code eCode{static_cast<int>(iRtnCode), std::generic_category()};
        throw std::system_error(eCode, g_mErrMap[iRtnCode]);
    }

    // Skip the first X number of inodes (these inodes may be prepopulated, containing important information).
    ext2_ino_t tInodeIdx = 0;
    struct ext2_inode_large tInode;
    do
    {
        iRtnCode = ext2fs_get_next_inode_full(tInodeScan, &tInodeIdx, reinterpret_cast<ext2_inode*>(&tInode), iSize);
        if (0 != iRtnCode)
        {
            std::error_code eCode{static_cast<int>(iRtnCode), std::generic_category()};
            throw std::system_error(eCode, g_mErrMap[iRtnCode]);
        }

        if (0 == tInodeIdx)
        {
            break;
        }

        // Reset the inode fields.
        tInode.i_atime = 1;
        tInode.i_mtime = 1;
        tInode.i_ctime = 1;
        tInode.i_crtime = 1;
        tInode.i_dtime = 0;
        tInode.i_atime_extra = 1;
        tInode.i_mtime_extra = 1;
        tInode.i_ctime_extra = 1;
        tInode.i_crtime_extra = 1;
        tInode.i_generation = 0x0;

        // Write the inode!
        iRtnCode = ext2fs_write_inode_full(atFileSys, tInodeIdx, reinterpret_cast<ext2_inode*>(&tInode), sizeof(iSize));
        if (0 != iRtnCode)
        {
            std::error_code eCode{static_cast<int>(iRtnCode), std::generic_category()};
            throw std::system_error(eCode, g_mErrMap[iRtnCode]);
        }
    } while (0 < tInodeIdx);

    // Close the scanner.
    if (nullptr != tInodeScan) { ext2fs_close_inode_scan(tInodeScan); }

    return iRtnCode;
}
