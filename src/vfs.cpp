#include "vfs.h"

/*
 * \brief OpenExt2Fs - Open EXT2 Filesystem
 *
 * This function will try to open a filesystem located at "apPath". This filesystem must be a valid Ext2/3/4 filesystem with a valid superblock and not errors.
 *
 * \param apPath String representing the filepath to the filesystem.
 *
 * \note Does not throw exceptions!
 * \return ext2_filsys object containing the open filesystem or nullptr if failed.
 */
ext2_filsys OpenExt2Fs(const char* apPath) noexcept
{
    // Sanity check.
    assert(nullptr != apPath);

    int iOpenFlags = EXT2_FLAG_EXCLUSIVE | EXT2_FLAG_64BITS;
    int iSuperblock = 0;
    unsigned int iBlkSz = 0;
    io_manager ioManager = unix_io_manager;
    ext2_filsys tFileSys;

    long iRtnCode =  ext2fs_open(apPath, iOpenFlags, iSuperblock, iBlkSz, ioManager, &tFileSys);
    if (0 == iRtnCode)
    {
        try
        {
            tFileSys->default_bitmap_type = EXT2FS_BMAP64_RBTREE;
            iRtnCode = ext2fs_read_bitmaps(tFileSys);
            if (0 != iRtnCode)
            {
                std::error_code eCode{static_cast<int>(iRtnCode), std::generic_category()};
                throw std::system_error(eCode, g_mErrMap[iRtnCode]);
            }

        }
        catch (std::system_error& eErr)
        {
            fprintf(stderr, "ERR> %s! Aborting...\n", eErr.what());
            if (nullptr != tFileSys)
            {
                ext2fs_close(tFileSys);
                tFileSys = nullptr;
            }
        }
    }
    else if (EXT2_ET_SB_CSUM_INVALID == iRtnCode)
    {
        fprintf(stderr, "ERR> Superblock checksum failed for \"%s\"! Aborting...\n", apPath);
        tFileSys = nullptr;
    }
    else
    {
        if (iRtnCode >= EXT2_ET_BASE)
        {
            fprintf(stderr, "ERR (%ld)> %s!\n", iRtnCode, g_mErrMap[iRtnCode].data());
        }
        else
        {
            fprintf(stderr, "ERR (%ld)> %s!\n", iRtnCode, strerror(iRtnCode));
        }

        fprintf(stderr, "ERR> Failed to open EXT2/3/4 VFS for \"%s\"! Aborting...\n", apPath);
        tFileSys = nullptr;
    }

    return tFileSys;
}

/*
 * \brief CloseExt2Fs - Closes an open VFS handle.
 * \param atFileSys Valid and open Ext2 FS handle to be closed.
 */
void CloseExt2Fs(ext2_filsys atFileSys)
{
    if (nullptr != atFileSys)
    {
        ext2fs_flush(atFileSys);
        ext2fs_close(atFileSys);
        ext2fs_free(atFileSys);
    }
}

/*
 * \brief CreateDirectory - Create a new directory inode and it's parent reference.
 *
 * This function will create a new inode for the directory as needed and link it to it's own name along with it's parent. This ensures that "." and ".." function correctly.
 *
 * \note This function will overwrite an existing inode and intern, abandon any links/data therein!
 * \note apOutInode will be overidden! Don't pass anything you want to keep!
 *
 * \param asDirName String representing the directory name.
 * \param atFileSys Valid and open Ext2 FS handle.
 * \param aiParent Index of the parent inode.
 * \param aiIndex Index of this inode.
 * \param apOutInode A valid ext2_inode_large handle that'll be populated with the new inode (used for linking).
 *
 * \exception std::system_error C++11 system_error containing error code and representing string.
 * \return Integer reflective of the ext2fs library return codes.
 */
long CreateDirectory(std::string asDirName, ext2_filsys atFileSys, ext2_ino_t aiParent, ext2_ino_t aiIndex, ext2_inode_large* apOutInode)
{
    assert(!asDirName.empty());
    assert(nullptr != atFileSys);
    assert(0 != aiParent);
    assert(0 != aiIndex);
    assert(nullptr != apOutInode);

    long iRtnCode = 1;

    // Create the new directory and flush it to disk.
    iRtnCode = ext2fs_mkdir(atFileSys, aiParent, aiIndex, asDirName.data());
    if (0 != iRtnCode)
    {
        std::error_code eCode{static_cast<int>(iRtnCode), std::generic_category()};
        throw std::system_error(eCode, g_mErrMap[iRtnCode]);
    }

    // Load up and populate apOutInode.
    const unsigned int ciSize = sizeof(ext2_inode_large);
    iRtnCode = ext2fs_read_inode_full(atFileSys, aiIndex, reinterpret_cast<ext2_inode*>(apOutInode), ciSize);
    if (0 != iRtnCode)
    {
        std::error_code eCode{static_cast<int>(iRtnCode), std::generic_category()};
        throw std::system_error(eCode, g_mErrMap[iRtnCode]);
    }

    return iRtnCode;
}

/*
 * \brief PopulatetVFS - Populate a newly created VFS.
 *
 * This function will take the currently open VFS handle and populate it with files in asListPath.
 *
 * \param asListPath String containing the path to the ordered file list to populate the VFS with.
 * \param atFileSys Valid and open Ext2 FS handle.
 * \param asRoot String containing the base "sysroot" path of the files specified in the file specified at asListPath.
 *
 * \exception std::system_error C++11 system_error containing error code and representing string.
 * \return Integer reflective of the ext2fs library return codes.
 */
long PopulatetVFS(std::string asListPath, ext2_filsys atFileSys, std::string asRoot)
{
    assert(nullptr != atFileSys);
    assert(!asListPath.empty());
    assert(!asRoot.empty());

    long iRtnCode = 1;
    ext2_ino_t tInodeIdx = EXT2_GOOD_OLD_FIRST_INO + 1;
    ext2_ino_t tRootInode = tInodeIdx;
    struct ext2_inode_large tInode;
    std::vector<std::string> lFileList = ExtractFiles(asListPath);

    // Iterate over the file list and begin allocating inodes and linking dentries!
    ext2_ino_t tPrevInode = tRootInode;
    for (std::vector<std::string>::iterator pIter = lFileList.begin(); pIter != lFileList.end(); ++pIter)
    {
        std::string sFilePath = (*pIter);
        if (!sFilePath.empty())
        {
            // Is this a child of root or another directory?
            if (sFilePath.find_first_of("/", 0) == sFilePath.find_last_of("/"))
            {
                // It's a root directory/entry, link to tRootInode.
                tPrevInode = tRootInode;
            }

            // Grab the entry's filetype.
            int iFileType = DetermineFileType(sFilePath, asRoot);
            if (0 <= iFileType && EXT2_FT_DIR != iFileType)
            {
                // Allocate blocks for the new inode.
                iRtnCode = CreateInode(atFileSys, tPrevInode, &tInodeIdx, &tInode);

                // Link the inodes!
                iRtnCode = ext2fs_link(atFileSys, tPrevInode, ExtractLastEntry(sFilePath).data(), tInodeIdx, iFileType);
                if (0 != iRtnCode)
                {
                    std::error_code eCode{static_cast<int>(iRtnCode), std::generic_category()};
                    throw std::system_error(eCode, g_mErrMap[iRtnCode]);
                }
            }
            else if (0 <= iFileType && EXT2_FT_DIR == iFileType)
            {
                // Create the directory instead.
                iRtnCode = CreateDirectory(ExtractLastEntry(sFilePath).data(), atFileSys, tPrevInode, tInodeIdx, &tInode);
            }
            else
            {
                std::error_code eCode{static_cast<int>(errno), std::generic_category()};
                throw std::system_error(eCode, strerror(errno));
            }

            // "Clone" the inode.
            CloneOwnerPerms(&tInode, sFilePath, asRoot);

            // Write the new inode.
            iRtnCode = WriteInode(atFileSys, tInode, tInodeIdx);

            // Set the previous inode.
            tPrevInode = tInodeIdx;
        }
    }

    // Just to be safe!
    iRtnCode = 0;

    return iRtnCode;
}

/*
 * \brief CreateNewVFS - Create a new Ext2 VFS on the open filesystem.
 *
 * This function will create a new VFS on the given ext2 filesystem handle.
 * \note This will discard and forcably override any existing superblock and table data!
 *
 * \param atFileSys  Valid and open Ext2 FS handle.
 *
 * \exception std::system_error C++11 system_error containing error code and representing string.
 * \return Integer reflective of the ext2fs library return codes.
 */
long CreateNewVFS(ext2_filsys atFileSys, std::string asFsName, const byte asUUID[16])
{
    assert(nullptr != atFileSys);

    long iRtnCode = 1;
    struct ext2_super_block tSuper;

    // Initalize a new VFS.
    iRtnCode = ext2fs_initialize(asFsName.data(), EXT2_FLAG_64BITS, &tSuper, unix_io_manager, &atFileSys);
    if (0 != iRtnCode)
    {
        std::error_code eCode{static_cast<int>(iRtnCode), std::generic_category()};
        throw std::system_error(eCode, g_mErrMap[iRtnCode]);
    }

    // Allocate tables.
    iRtnCode = ext2fs_allocate_tables(atFileSys);
    if (0 != iRtnCode)
    {
        std::error_code eCode{static_cast<int>(iRtnCode), std::generic_category()};
        throw std::system_error(eCode, g_mErrMap[iRtnCode]);
    }

    // Set the UUID.
    memcpy(tSuper.s_uuid, asUUID, 16);

    // Copy superblock to filesystem handle.
    memcpy(atFileSys->super, &tSuper, sizeof(struct ext2_super_block));

    // Reset timestamps.
    iRtnCode = ResetSuperblockTimestamps(atFileSys);
    if (0 != iRtnCode)
    {
        std::error_code eCode{static_cast<int>(iRtnCode), std::generic_category()};
        throw std::system_error(eCode, g_mErrMap[iRtnCode]);
    }

    // (Re)Set HTREE hash.
    unsigned int const cilHTreeSeed[4] ={0xaaaaaaaa, 0xbbbbbbbb, 0xcccccccc, 0xdddddddd};
    iRtnCode = SetHTreeSeed(atFileSys, cilHTreeSeed);
    if (cilHTreeSeed != atFileSys->super->s_hash_seed)
    {
        std::error_code eCode{static_cast<int>(iRtnCode), std::generic_category()};
        throw std::system_error(eCode, g_mErrMap[iRtnCode]);
    }

    // Allocate reserved inodes.
    iRtnCode = AllocateReservedInodes(atFileSys);
    if (0 != iRtnCode)
    {
        std::error_code eCode{static_cast<int>(iRtnCode), std::generic_category()};
        throw std::system_error(eCode, g_mErrMap[iRtnCode]);
    }

    // Create lost+found.
    iRtnCode = CreateLostAndFound(atFileSys);
    if (0 != iRtnCode)
    {
        std::error_code eCode{static_cast<int>(iRtnCode), std::generic_category()};
        throw std::system_error(eCode, g_mErrMap[iRtnCode]);
    }

    // Flush the new VFS!
    iRtnCode = ext2fs_flush(atFileSys);
    if (0 != iRtnCode)
    {
        std::error_code eCode{static_cast<int>(iRtnCode), std::generic_category()};
        throw std::system_error(eCode, g_mErrMap[iRtnCode]);
    }

    // DONE!
    return iRtnCode;
}

/*
 * \brief AllocateReservedInodes - Allocate new reserved inodes for a new VFS.
 *
 * This function will allocate a new set of reserved inodes. These inodes are as follows:
 * 1 - Bad Blocks inode
 * 2 - Root Inode
 * 3 - ACL Index inode (deprecated/unused)
 * 4 - ACL data inode (deprecated/unused)
 * 5 - Boot loader inode
 * 6 - Undeleted directory inode
 * 7 - Resize inode
 * 8 - Journal inode (unused)
 * 9 - Excluded blocks inode
 * 10 - Replicated blocks inode
 *
 * Inode 11 (EXT2_GOOD_OLD_FIRST_INO) is the first inode that can used by the regular filesystem.
 *
 * \param atFileSys  Valid and open Ext2 FS handle.
 *
 * \exception std::system_error C++11 system_error containing error code and representing string.
 * \return Integer reflective of the ext2fs library return codes.
 */
long AllocateReservedInodes(ext2_filsys atFileSys)
{
    assert(nullptr != atFileSys);

    long iRtnCode = 1;
    ext2_ino_t iCurrInode = 0;
    ext2_inode_large tInode;

    // Begin creating inodes 1-10, with a special case for 2 (EXT2_ROOT_INO).
    const size_t ciNumReserved = EXT4_REPLICA_INO;
    for (size_t iIdx = 1; iIdx <= ciNumReserved; ++iIdx)
    {
        // Allocate a new inode.
        if (iIdx != 2)
        {
            iRtnCode = CreateInode(atFileSys, 0, &iCurrInode, &tInode);
        }
        else
        {
            iRtnCode = CreateRoot(atFileSys, &tInode);
        }

        if (0 != iRtnCode)
        {
            std::error_code eCode{static_cast<int>(iRtnCode), std::generic_category()};
            throw std::system_error(eCode, g_mErrMap[iRtnCode]);
        }
        else if (iCurrInode >= EXT2_GOOD_OLD_FIRST_INO)
        {
            // SOMETHING IS WRONG! WE SHOULDN'T HAVE INODES THIS HIGH YET!
            char* pErrStr = new char[256];
            memset(pErrStr, 0, 256);
            snprintf(pErrStr, 255, "CORRUPTION: We have an inode index ABOVE EXT2_GOOD_OLD_FIRST_INO! (Should be: %lu, but we have: %u)", iIdx, iCurrInode);

            std::error_code eCode{static_cast<int>(iRtnCode), std::generic_category()};
            throw std::system_error(eCode, pErrStr);
        }
        // else : All good, continue as normal.
    }

    // DONE!
    return iRtnCode;
}

/*
 * \brief CreateLostAndFound - Create "lost+found" for the filesystem.
 *
 * This function will create the lost+found directory for the filesystem and set it's permissions/ownership correctly.
 *
 * \param atFileSys  Valid and open Ext2 FS handle.
 *
 * \exception std::system_error C++11 system_error containing error code and representing string.
 * \return Integer reflective of the ext2fs library return codes.
 */
long CreateLostAndFound(ext2_filsys atFileSys)
{
    assert(nullptr != atFileSys);

    long iRtnCode = 1;
    ext2_ino_t iCurrInode = EXT2_GOOD_OLD_FIRST_INO;
    ext2_inode_large tInode;

    // Create lost+found (EXT2_GOOD_OLD_FIRST_INO)
    iRtnCode = CreateDirectory("lost+found", atFileSys, EXT2_ROOT_INO, iCurrInode, &tInode);
    if (0 != iRtnCode)
    {
        std::error_code eCode{static_cast<int>(iRtnCode), std::generic_category()};
        throw std::system_error(eCode, g_mErrMap[iRtnCode]);
    }

    // Set the ownership and permissions correctly.
    tInode.i_mode = 0x00; // Zero this out so we don't have any cross-contamination.
    tInode.i_uid = 0x00;
    tInode.i_gid = 0x00;

    tInode.i_mode = 0x4000 | 0x0100 | 0x0080 | 0x0040; // (S_IFDIR | S_IRUSR | S_IWUSR | S_IXUSR)

    // Write the inode.
    iRtnCode = WriteInode(atFileSys, tInode, iCurrInode, true);
    if (0 != iRtnCode)
    {
        std::error_code eCode{static_cast<int>(iRtnCode), std::generic_category()};
        throw std::system_error(eCode, g_mErrMap[iRtnCode]);
    }

    // DONE!
    return iRtnCode;
}

/*
 * \brief PurgeInodes - Remove ALL inodes from filesystem.
 *
 * This function will systematically go through all the inodes in the system and remove them. This is needed so the filesystem is clean.
 * \note This will REMOVE the root inode! Be sure to call "CreateRoot()" to create a new root inode!
 *
 * \param atFileSys Valid and open Ext2 FS handle.
 *
 * \exception std::system_error C++11 system_error containing error code and representing string.
 * \return Integer reflective of the ext2fs library return codes.
 */
long PurgeInodes(ext2_filsys atFileSys)
{
    assert(nullptr != atFileSys);

    long iRtnCode = 1;
    ext2_inode_scan tInodeScan = nullptr;

    // Begin iterating over the list and creating ordered inodes!
    iRtnCode = ext2fs_open_inode_scan(atFileSys, 0, &tInodeScan);
    if (0 != iRtnCode)
    {
        std::error_code eCode{static_cast<int>(iRtnCode), std::generic_category()};
        throw std::system_error(eCode, g_mErrMap[iRtnCode]);
    }

    // Skip the first X number of inodes (these inodes may be prepopulated, containing important information).
    const unsigned int ciSize = sizeof(ext2_inode_large);
    ext2_ino_t tInodeIdx = 0;
    ext2_ino_t tRootInode = 0xffffffff;
    struct ext2_inode_large tInode;
    do
    {
        // Grab the next inode!
        iRtnCode = ext2fs_get_next_inode_full(tInodeScan, &tInodeIdx, reinterpret_cast<ext2_inode*>(&tInode), ciSize);
        if (0 != iRtnCode)
        {
            std::error_code eCode{static_cast<int>(iRtnCode), std::generic_category()};
            throw std::system_error(eCode, g_mErrMap[iRtnCode]);
        }

        // Check if this is the root inode.
        if (0xffffffff <= tRootInode)
        {
            // Grab the dentry associated with this inode (we're looking for "/").
            char* pPath = new char[256];
            memset(pPath, 0, sizeof(char)*256);

            iRtnCode = ext2fs_get_pathname(atFileSys, EXT2_ROOT_INO, tInodeIdx, &pPath);
            if (0 != iRtnCode)
            {
                std::error_code eCode{static_cast<int>(iRtnCode), std::generic_category()};
                throw std::system_error(eCode, g_mErrMap[iRtnCode]);
            }

            if (!strcmp(pPath, "/"))
            {
                tRootInode = tInodeIdx;
            }

            if (nullptr != pPath) { delete[] pPath; }
        }
        else if (EXT2_GOOD_OLD_FIRST_INO <= tInodeIdx && 0x00 != tInode.i_mode && 0x00 != tInode.i_blocks)
        {
            // Remove the inode!
            tInode.i_dtime = 1; // This marks the file for deletion.

            // Remove the blocks!
            if (ext2fs_inode_has_valid_blocks2(atFileSys, reinterpret_cast<ext2_inode*>(&tInode)))
            {
                blk64_t iLastCluster = 0;
                ext2fs_block_iterate3(atFileSys, tInodeIdx, BLOCK_FLAG_READ_ONLY, nullptr, [](ext2_filsys aFs, blk64_t *pBlocknr, e2_blkcnt_t, blk64_t, int, void *apPrivate) -> int {
                    blk64_t	iBlock = *pBlocknr;
                    blk64_t *ipLastCluster = static_cast<blk64_t *>(apPrivate);
                    blk64_t iCluster = EXT2FS_B2C(aFs, iBlock);

                    if (iCluster != *ipLastCluster)
                    {
                        *ipLastCluster = iCluster;

                        ext2fs_block_alloc_stats2(aFs, iBlock, -1);
                    }

                    return 0;
                }, &iLastCluster);
            }

            // Unmark the inode bitmap!
            iRtnCode = ext2fs_unmark_inode_bitmap(atFileSys->inode_map, tInodeIdx);
            if (0 != iRtnCode)
            {
                std::error_code eCode{static_cast<int>(iRtnCode), std::generic_category()};
                throw std::system_error(eCode, g_mErrMap[iRtnCode]);
            }

            // Write this inode and flush to the disk!
            iRtnCode = WriteInode(atFileSys, tInode, tInodeIdx, true);
        }
    } while (0 != tInodeIdx && atFileSys->super->s_first_ino > tInodeIdx);

    // Remove the root inode!
    iRtnCode = ext2fs_read_inode_full(atFileSys, tRootInode, reinterpret_cast<ext2_inode*>(&tInode), ciSize);
    if (0 != iRtnCode)
    {
        std::error_code eCode{static_cast<int>(iRtnCode), std::generic_category()};
        throw std::system_error(eCode, g_mErrMap[iRtnCode]);
    }

    tInode.i_dtime = 1; // This marks the file for deletion.

    // Remove the blocks!
    if (ext2fs_inode_has_valid_blocks2(atFileSys, reinterpret_cast<ext2_inode*>(&tInode)))
    {
        blk64_t iLastCluster = 0;
        ext2fs_block_iterate3(atFileSys, tInodeIdx, BLOCK_FLAG_READ_ONLY, nullptr, [](ext2_filsys aFs, blk64_t *pBlocknr, e2_blkcnt_t, blk64_t, int, void *apPrivate) -> int {
            blk64_t	iBlock = *pBlocknr;
            blk64_t *ipLastCluster = static_cast<blk64_t *>(apPrivate);
            blk64_t iCluster = EXT2FS_B2C(aFs, iBlock);

            if (iCluster != *ipLastCluster)
            {
                *ipLastCluster = iCluster;

                ext2fs_block_alloc_stats2(aFs, iBlock, -1);
            }

            return 0;
        }, &iLastCluster);
    }

    // Unmark the inode bitmap!
    iRtnCode = ext2fs_unmark_inode_bitmap(atFileSys->inode_map, tRootInode);
    if (0 != iRtnCode)
    {
        std::error_code eCode{static_cast<int>(iRtnCode), std::generic_category()};
        throw std::system_error(eCode, g_mErrMap[iRtnCode]);
    }

    // Write this inode and flush to the disk!
    iRtnCode = WriteInode(atFileSys, tInode, tRootInode, true);

    // Close the scanner.
    if (nullptr != tInodeScan) { ext2fs_close_inode_scan(tInodeScan); }

    return iRtnCode;
}
