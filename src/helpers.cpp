#include "helpers.h"

/*
 * \brief ExtractFiles - Extract list from file.
 *
 * This function will attempt to open the file located at asListPath and read in the file list to be synchronized.
 * Each line represents a different entry to be synchronized.
 *
 * \param asListPath String holding the path to the file to be read.
 *
 * \exception std::system_error C++11 system_error containing error code and representing string.
 * \return std::vector<std::string> of entries to be synchronized.
 */
std::vector<std::string> ExtractFiles(std::string asListPath)
{
    assert(!asListPath.empty());

    std::vector<std::string> lFileList;

    std::fstream hFile;
    hFile.open(asListPath);

    if (hFile.is_open())
    {
        for (std::string sFileLine; !hFile.eof(); std::getline(hFile, sFileLine))
        {
            if (!sFileLine.empty())
            {
                lFileList.push_back(sFileLine);
            }
        }
        hFile.close();
    }
    else
    {
        std::error_code eCode{255, std::generic_category()};
        throw std::system_error(eCode, std::string("Opened file is not opened?!"));
    }

    return lFileList;
}

/*
 * \brief DetermineFileType - Determine a synch entry's file type.
 *
 * This function will attempt to stat() the provided path located in the provided root and determine the filetype the new inode/dentry should be.
 *
 * \param asFilePath - The file name to be stat'd
 * \param asRoot - The root to the "sysroot" of the file (must be a directory!)
 *
 * \exception std::system_error C++11 system_error containing error code and representing string.
 * \return Integer code representing the underlying libext2fs type, or -1 if failure.
 */
int DetermineFileType(std::string asFilePath, std::string asRoot)
{
    assert(!asFilePath.empty());
    assert(!asRoot.empty());

    int iEntryType = -1;

    // Format the final path to stat().
    std::string sFinalPath = asRoot + asFilePath;

    // Stat the entry!
    struct stat tEntryStat;
    if (0 != stat(sFinalPath.data(), &tEntryStat))
    {
        std::error_code eCode{errno, std::generic_category()};
        throw std::system_error(eCode, strerror(errno));
    }

    // Extract the mode and type.
    if (S_ISREG(tEntryStat.st_mode)) { iEntryType = EXT2_FT_REG_FILE; }
    else if (S_ISDIR(tEntryStat.st_mode)) { iEntryType = EXT2_FT_DIR; }
    else if (S_ISCHR(tEntryStat.st_mode)) { iEntryType = EXT2_FT_CHRDEV; }
    else if (S_ISBLK(tEntryStat.st_mode)) { iEntryType = EXT2_FT_BLKDEV; }
    else if (S_ISFIFO(tEntryStat.st_mode)) { iEntryType = EXT2_FT_FIFO; }
    else if (S_ISSOCK(tEntryStat.st_mode)) { iEntryType = EXT2_FT_SOCK; }
    else if (S_ISLNK(tEntryStat.st_mode)) { iEntryType = EXT2_FT_SYMLINK; }
    else { iEntryType = EXT2_FT_UNKNOWN; }

    return iEntryType;
}

/*
 * \brief ExtractLastEntry - Extracts the last element in a path.
 *
 * This function will extract the last element from a path by using the '/' character as a delimiter.
 *
 * \param asPath String of the path to parse.
 *
 * \exception std::system_error C++11 system_error containing error code and representing string.
 * \return std::string containing the last element or an empty string if failed.
 */
std::string ExtractLastEntry(std::string asPath) noexcept
{
    assert(!asPath.empty());

    std::string sLastElement = "";

    if (asPath.at(asPath.size()-1) == '/')
    {
        asPath.erase(asPath.end()-1);
    }

    size_t iLastSlash = asPath.find_last_of("/");
    if (iLastSlash != std::string::npos)
    {
        sLastElement = asPath.substr(iLastSlash+1);
    }
    else
    {
        sLastElement = asPath;
    }

    return sLastElement;
}
