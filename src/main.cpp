#include "conext2.h"

void PrintUsage()
{
    fprintf(stdout, "Ext2 Inode Consistency Utility\n"
                    "Usage:\n"
                    "    conext2 /path/to/device /path/to/file.list [source_timestamp]\n"
                    "\n"
                    "Version: %d.%d r%d (%x)\n"
                    "(c) 2019 Sockeye Software\n"
                    "Author: Travis M. Ervin <travis@sockeyesfotware.com>\n", MAJOR_VER(CONEXT2_VERSION), MINOR_VER(CONEXT2_VERSION), REV_VER(CONEXT2_VERSION), CONEXT2_VERSION);
}

int main(int argc, char* argv[])
{
    int iRtnCode = 1;
    std::string sDevice = "";
    std::string sPath = "";
    unsigned int iTimestamp = 0;

    if (1 < argc)
    {
        if (!strcmp(argv[1], "-h") || !strcmp(argv[1], "--help"))
        {
            PrintUsage();
            return 0;
        }
        else
        {
            sDevice = argv[1];

            if (3 <= argc)
            {
                sPath = argv[2];
            }

            if (4 <= argc)
            {
                iTimestamp = static_cast<unsigned int>(atoi(argv[3]));
            }
        }

        if (!sDevice.empty() && !sPath.empty())
        {
            // Check and make sure the device we want is available.
            struct stat tStat;
            if (0 != stat(sDevice.data(), &tStat))
            {
                fprintf(stderr, "ERR (%d)> %s!\n", errno, strerror(errno));
                iRtnCode = errno;
            }
            else
            {
                ext2_filsys tFS = OpenExt2Fs(sDevice.data());
                if (nullptr != tFS)
                {
                    try
                    {
                        // Create a new VFS.
                        const byte csUUID[16] = {0x0, 0x1, 0x2, 0x3, 0x4, 0x5, 0x6, 0x7, 0x8, 0x9, 0xa, 0xa, 0xb, 0xc, 0xd, 0xe};
                        CreateNewVFS(tFS, "Test", csUUID);
                        PopulatetVFS("/tmp/dummy.list", tFS, "/tmp/dummy");
                    }
                    catch(std::system_error const& eExcept)
                    {
                        fprintf(stderr, "ERR> %s!\n", eExcept.what());
                    }

                    CloseExt2Fs(tFS);
                }
            }
        }
        else
        {
            fprintf(stderr, "ERR (1)> You must provide a device path AND inode path! See \"--help\" for usage.\n");
            iRtnCode = 1;
        }
    }
    else
    {
        fprintf(stderr, "ERR (1)> Invalid number of arguments! See \"--help\" for usage.\n");
        iRtnCode = 1;
    }

    return iRtnCode;
}
