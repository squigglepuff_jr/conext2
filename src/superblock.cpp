#include "superblock.h"

/*
 * \brief ResetSuperblockTimestamps - Reset timestamps for the provided inode.
 *
 * This function will reset the timestamps for the provided superblock to 1.
 * \note Resetting to 0 causes the underlying library to auto-insert the current timestamp.
 *
 * \param atFileSys Valid and open Ext2 FS handle.
 *
 * \exception std::system_error C++11 system_error containing error code and representing string.
 * \return Integer reflective of the ext2fs library return codes.
 */
long ResetSuperblockTimestamps(ext2_filsys atFileSys)
{
    assert(nullptr != atFileSys);

    long iRtnCode = 1;

    // Reset the fields.
    atFileSys->super->s_mkfs_time = 1;
    atFileSys->super->s_mtime = 1;
    atFileSys->super->s_wtime = 1;
    atFileSys->super->s_lastcheck = 1;
    atFileSys->super->s_checkinterval = 0xffffffff; // Max time.
    atFileSys->super->s_mtime_hi = 0x00;
    atFileSys->super->s_wtime_hi = 0x00;
    atFileSys->super->s_mkfs_time_hi = 0x00;
    atFileSys->super->s_lastcheck_hi = 0x00;

    // Flush the filesystem.
    iRtnCode = ext2fs_flush(atFileSys);
    if (0 != iRtnCode)
    {
        std::error_code eCode{static_cast<int>(iRtnCode), std::generic_category()};
        throw std::system_error(eCode, g_mErrMap[iRtnCode]);
    }

    return iRtnCode;
}

/*
 * \brief SetHTreeSeed - Set the HTREE seed for directory hashing.
 *
 * This function will set the superblock's HTREE seed (16-bytes/128-bits) and flush the superblock data to the filesystem object.
 *
 * \param atFileSys Valid and open Ext2 FS handle.
 * \param alSeed Array of 4 unsigned integers to set the HTREE seed to.
 *
 * \exception std::system_error C++11 system_error containing error code and representing string.
 * \return Integer reflective of the ext2fs library return codes.
 */
long SetHTreeSeed(ext2_filsys atFileSys, unsigned int const alSeed[4])
{
    assert(nullptr != atFileSys);
    assert(nullptr != alSeed);

    long iRtnCode = 1;
    const size_t ciArrSz = sizeof(unsigned int) * 4;

    memcpy(atFileSys->super->s_hash_seed, alSeed, ciArrSz);
    if (0 != errno)
    {
        std::error_code eCode{errno, std::generic_category()};
        throw std::system_error(eCode, strerror(errno));
    }

    // Flush the filesystem.
    iRtnCode = ext2fs_flush(atFileSys);
    if (0 != iRtnCode)
    {
        std::error_code eCode{static_cast<int>(iRtnCode), std::generic_category()};
        throw std::system_error(eCode, g_mErrMap[iRtnCode]);
    }

    return iRtnCode;
}
