# ALPHA
## Version 0.1 r1
* ADD: Versioning to headers/API.
* ADD: Added new VFS initialization functions.
* TODO: Test new VFS creation and iron out bugs.

## Version 0.1 r0
* ADD: Inode module for inode specific functions.
* ADD: Superblock module for superblock specific functions.
* ADD: VFS module for overall VFS functions.
* ADD: Helpers module containing utility functions.
* CFG: Configured CMakeList.txt to be easier to modify in future.
